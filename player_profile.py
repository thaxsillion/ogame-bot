from typing import Optional

from resources import Resources
from buildable import Buildable, BuildableType


class PlayerProfile():
    username: Optional[str] = None
    home_planet: Optional[str] = None
    guild: Optional[str] = None
    points_buildings: Optional[str] = None
    rank_buildings: Optional[str] = None
    points_research: Optional[str] = None
    rank_research: Optional[str] = None
    points_fleet: Optional[str] = None
    rank_fleet: Optional[str] = None
    points_defense: Optional[str] = None
    rank_defense: Optional[str] = None
    sum_points: Optional[str] = None
    sum_rank: Optional[str] = None

    def description(self):
        return u'Player (%s) rank: %s' % (self.username if self.username is not None else 'Unknown',
                                          self.sum_rank if self.sum_rank is not None else '-1')

    def __str__(self):
        return self.description()

    def __repr__(self):
        return self.description()
