#!/usr/local/bin/python3
# -*- coding: UTF-8 -*-
import re
import sqlite3
from game import Game
from game_credentials import GameCredentials
import sys

from game_url import GameUrl

is_headless = (bool(sys.argv[3] == '--headless') if len(sys.argv) > 3 else False)
conn = sqlite3.connect('players.db')

game = Game(credentials=GameCredentials(username=sys.argv[1], password=sys.argv[2]), is_headless=is_headless)
game.prepare()

c = conn.cursor()

# Create table
c.execute('''CREATE TABLE IF NOT EXISTS players
             (name text, galaxy integer, solar integer, planet integer, points integer)''')

# 151
for i in range(686, 3000):
    print("Fetching player with id %d" % i)
    game.game_driver.navigate_to_url(GameUrl.PLAYER_PROFILE, i)
    player = game.game_driver.extract_player_profile()
    match = re.search("\[(?P<galaxy>\d+):(?P<solar>\d+):(?P<planet>\d+)\]", player.home_planet)
    if match is None:
        print("Fail!")
        continue
    galaxy = int(match.group('galaxy'))
    solar = int(match.group('solar'))
    planet = int(match.group('planet'))
    points = int(player.sum_points.replace('.', ''))
    c.execute("INSERT INTO players (name, galaxy, solar, planet, points) VALUES (?,?,?,?,?)",
              (player.username, galaxy, solar, planet,
               points))
    print("Success! Player %s [%d:%d:%d] points: %s" % (player.username, galaxy, solar, planet, points))

conn.commit()
conn.close()
