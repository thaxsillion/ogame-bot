import datetime
from typing import List
from enum import Enum


class LogLevel(Enum):
    WARNING = 1
    DEFAULT = 2
    SUCCESS = 3


class LoggerImplementation:
    def __init__(self):
        pass

    def log(self, message: str, end: str = '\r\n'):
        pass


class Logger:
    loggers: List[LoggerImplementation] = []

    def log(self, message: str, log_level = LogLevel.DEFAULT, end: str = '\r\n'):
        timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        output = '[%s] %s' % (timestamp, message.strip())

        for logger in self.loggers:
            logger.log(output, log_level, end)

    def add_logger(self, logger: LoggerImplementation):
        self.loggers.append(logger)


class PrintLogger(Logger):
    def __init__(self):
        pass

    def color_for_log_level(self, log_level: LogLevel) -> str:
        colors = {
            LogLevel.DEFAULT: '',
            LogLevel.WARNING: '\033[91m',
            LogLevel.SUCCESS: '\033[92m'
        }
        return colors[log_level]

    def log(self, message: str, log_level = LogLevel.DEFAULT, end: str = '\r\n'):
        prefix = self.color_for_log_level(log_level)
        suffix = '\033[0m' if len(prefix) > 0 else ''
        print(prefix + message + suffix, end=end)


class FileLogger(Logger):
    def __init__(self):
        pass

    def log(self, message: str, log_level = LogLevel.DEFAULT, end: str = '\r\n'):
        with open('logs.txt', 'a', encoding='utf8') as file:
            file.write(message + "\r\n")
