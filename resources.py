class Resources:
    crystals: int = 0
    deuterium: int = 0
    metal: int = 0
    energy: int = 0

    def __init__(self, crystals: int, deuterium: int, metal: int, energy: int):
        self.crystals = crystals
        self.deuterium = deuterium
        self.metal = metal
        self.energy = energy

    def description(self):
        return u'Crystals: %.1f Deuterium: %.1f, Metal: %.1f, Energy used %d%%' % (self.crystals, self.deuterium,
                                                                                   self.metal, self.energy)

    def human_format(self, **options):
        resource_dict = {'Metal': self.metal, 'Crystal': self.crystals, 'Deuterium': self.deuterium}

        include_zeros = options['include_zeros'] or True
        filtered_dict = dict((k, v) for k, v in resource_dict.items() if (not include_zeros and v > 0) or (include_zeros and v >= 0))
        return ', '.join(["%s: %d" % (k, v) for k, v in filtered_dict.items()])

    def __str__(self):
        return self.description()

    def __repr__(self):
        return self.description()

    def __sub__(self, other):
        return Resources(self.crystals - other.crystals,
                         self.deuterium - other.deuterium,
                         self.metal - other.metal,
                         self.energy - other.energy)

    def __ge__(self, other):
        return self.energy >= other.energy and self.crystals >= other.crystals and self.metal >= other.metal and self.deuterium >= other.deuterium

    def __le__(self, other):
        return self.energy <= other.energy and self.crystals <= other.crystals and self.metal <= other.metal and self.deuterium <= other.deuterium

    def __eq__(self, other):
        return self.energy == other.energy and self.crystals == other.crystals and self.metal == other.metal and self.deuterium == other.deuterium

    def __gt__(self, other):
        return self.energy > other.energy and self.crystals > other.crystals and self.metal > other.metal and self.deuterium > other.deuterium

    def __ne__(self, other):
        return self.energy != other.energy or self.crystals != other.crystals or self.metal != other.metal or self.deuterium != other.deuterium
