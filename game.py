import datetime
import time

from threading import Thread
from game_url import GameUrl
from game_driver import GameDriver
from default_game_driver import DefaultGameDriver
from game_credentials import GameCredentials
from planet import Planet
from resource_calculator import ResourceCalculator
from buildable import Buildable, BuildableType
from build_order_loader import BuildOrderLoader
from build_order_item import BuildOrderItem
from build_queue_item import BuildQueueItem
from building import Building
from research import Research
from resources import Resources
from time_formatter import TimeFormatter
from typing import Dict, Tuple, List, Optional
from logger import Logger, PrintLogger, FileLogger, LogLevel


class Game:
    logger: Logger = Logger()
    game_driver: Optional[GameDriver] = None
    credentials: Optional[GameCredentials] = None

    build_orders: [BuildOrderItem] = []
    current_planet: Optional[Planet] = None
    planets: List[Planet] = []
    researches: [Research] = []

    def __init__(self, credentials: GameCredentials, is_headless: bool):
        self.credentials = credentials
        self.setup_driver(is_headless)

    def setup_driver(self, is_headless: bool):
        self.game_driver = DefaultGameDriver(is_headless=is_headless)

    def prepare(self):
        self.logger.add_logger(PrintLogger())
        self.logger.add_logger(FileLogger())

        self.game_driver.navigate_to_url(GameUrl.MAIN)
        self.login()

        self.game_driver.wait_for_page()

        self.planets = self.game_driver.detect_planets()

        for planet in self.planets:
            planet.build_orders = BuildOrderLoader(logger=self.logger).build_order_for_planet(planet)

        self.logger.log('Detected %d planets: %s' % (len(self.planets), ', '.join(map(lambda p: p.name, self.planets))))

        self.game_driver.dump_cookies()

    def start_battle_hall_check(self):
        thread = Thread(target=self.battle_hall_executor)
        thread.start()

    def battle_hall_executor(self):
        battle_hall_entries = []

        while True:
            new_entries = self.game_driver.get_battle_hall()

            if len(battle_hall_entries) > 0:
                new_elements = new_entries - battle_hall_entries
                for element in new_elements:
                    print('New battle hall entry: %s' + element)
            time.sleep(60)

    def start_game_loop(self):
        self.start_battle_hall_check()

        while False:
            for planet in self.planets:
                self.current_planet = planet
                self.game_driver.navigate_to_planet(planet)
                self.game_driver.wait_for_page()

                try:
                    self.check_buildings()
                    self.check_resources()

                    if not self.current_planet.is_satellite:
                        self.check_researches()

                    build_order_item = planet.get_next_build_order_item()

                    if build_order_item and planet.can_build(build_order_item.type):
                        buildable = planet.buildable_from_build_order_item(build_order_item)

                        if planet.has_available_resources(buildable.upgrade_costs):
                            self.logger.log("[%s] Starting %s %d" % (
                                planet.name, buildable.name, buildable.level + 1), LogLevel.SUCCESS)
                            self.build(buildable)
                        else:
                            buildable_advanced = buildable
                            buildable_advanced.level += 1

                            resource_diff = buildable_advanced.upgrade_costs - planet.resources

                            insufficient_message = 'Insufficient resources for %s.' % \
                                                   buildable_advanced.short_description()
                            insufficient_message += ' Remaining ' + resource_diff.human_format(
                                include_zeros=False) + '.'

                            rps = planet.resources_per_second

                            if rps:
                                seconds_required = ResourceCalculator.seconds_diff(rps, resource_diff)
                                insufficient_message += ' ' + TimeFormatter.eta_from_seconds(seconds_required) + '.'
                            self.logger.log('[%s] %s' % (planet.name, insufficient_message))
                    else:
                        next_item = planet.get_next_build_order_item()
                        if next_item:
                            self.logger.log(
                                '[%s] Building %s %d ... Next build item: %s %d' % (planet.name,
                                                                                    build_order_item.name,
                                                                                    build_order_item.level,
                                                                                    next_item.name, next_item.level))
                        else:
                            self.logger.log('[%s] No next item in build queue.' % planet.name, LogLevel.WARNING)
                except Exception as exception:
                    self.logger.log(str(exception))
                    self.logger.log('[%s] Failed to watch production' % planet.name)
                time.sleep(4)
            time.sleep(30)

    def build(self, buildable: Buildable):
        if buildable.type == BuildableType.BUILDING:
            self.game_driver.navigate_to_url(GameUrl.BUILDINGS)
        elif buildable.type == BuildableType.RESEARCH:
            self.game_driver.navigate_to_url(GameUrl.RESEARCH)

        built = self.game_driver.build(buildable)
        if not built:
            self.logger.log('[%s] Could not build %s' % (self.current_planet.name, buildable.name), LogLevel.WARNING)

    def check_build_queue(self, queue_type: BuildableType):
        self.current_planet.build_queue[queue_type] = self.game_driver.get_build_queue(queue_type)

        self.logger.log('[%s] %d items in %s build queue' % (
            self.current_planet.name, len(self.current_planet.build_queue[queue_type]), queue_type.value))

    def check_buildings(self):
        self.game_driver.navigate_to_url(GameUrl.BUILDINGS)
        self.check_build_queue(BuildableType.BUILDING)

        self.current_planet.buildings = self.game_driver.get_buildings()

    def check_researches(self):
        self.game_driver.navigate_to_url(GameUrl.RESEARCH)
        self.check_build_queue(BuildableType.RESEARCH)

        self.researches = self.game_driver.get_researches()

    def check_resources(self):
        if not self.current_planet:
            return

        resources = self.game_driver.get_resources()
        if resources:
            self.current_planet.set_current_resources(resources)

    def login(self):
        self.game_driver.login(self.credentials.username, self.credentials.password)
