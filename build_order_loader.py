import csv
import os.path
from planet import Planet
from build_order_item import BuildOrderItem
from buildable import BuildableType
from logger import Logger
from typing import List, Optional


class BuildOrderLoader:
    DEFAULT_DIR = 'build_orders/'

    logger: Optional[Logger] = None

    def __init__(self, logger: Logger):
        self.logger = logger

    def path_to_build_order(self, name: str) -> str:
        return BuildOrderLoader.DEFAULT_DIR + name + '.txt'

    def build_order_for_planet(self, planet: Planet) -> List[BuildOrderItem]:
        build_orders = []

        filepath = self.path_to_build_order('buildorder')

        planet_filepath = self.path_to_build_order('buildorder-' + planet.name)
        if os.path.isfile(planet_filepath):
            filepath = planet_filepath

        self.logger.log('[%s] Using build order at path: %s' % (planet.name, filepath))

        with open(filepath, 'r', encoding="utf-8") as line:
            orders = csv.reader(line, delimiter=',')

            for order in orders:
                buildable_type = BuildableType[order[0]]

                if buildable_type:
                    build_order = BuildOrderItem(buildable_type, order[1], int(order[2]))
                    build_orders.append(build_order)
                else:
                    print('Could not parse %s' % order[0])
        return build_orders
