from buildable import BuildableType
from typing import Optional


class BuildOrderItem:
    type: Optional[BuildableType] = None
    name: str = u''
    level: int = 0

    def __init__(self, buildable_type: BuildableType, name: str, level: int):
        self.type = buildable_type
        self.name = name
        self.level = level
