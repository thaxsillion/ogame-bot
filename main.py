#!/usr/local/bin/python3
# -*- coding: UTF-8 -*-

from game import Game
from game_credentials import GameCredentials
import sys

is_headless = (bool(sys.argv[3] == '--headless') if len(sys.argv) > 3 else False)

game = Game(credentials=GameCredentials(username=sys.argv[1], password=sys.argv[2]), is_headless=is_headless)
game.prepare()
game.start_game_loop()
