import os
from enum import Enum


class GameUrl(Enum):
    MAIN = 'https://mirkogame.pl/'
    BUILDINGS = 'https://mirkogame.pl/game.php?page=buildings'
    RESEARCH = 'https://mirkogame.pl/game.php?page=research'
    SWITCH_PLANET = 'https://mirkogame.pl/game.php?page=overview&cp=%d'
    OVERVIEW = 'https://mirkogame.pl/game.php?page=overview'
    PLAYER_PROFILE = 'https://mirkogame.pl/game.php?page=playerCard&id=%d'

    TEST_EXTRACT_PLANETS = 'file:///' + os.getcwd() + "//" + 'test/extract_planets.html'
    TEST_PLAYER_PROFILE = 'file:///' + os.getcwd() + "//" + 'test/extract_player_profile.html'