from building import Building
from research import Research
from build_order_item import BuildOrderItem
from build_queue_item import BuildQueueItem
from buildable import Buildable, BuildableType
from resources import Resources
from resource_calculator import ResourceCalculator
from typing import Dict, Tuple, List, Optional
import datetime


class Planet:
    id: Optional[int] = None
    name: str = ''
    coords: str = ''
    is_satellite: bool = False

    buildings: [Building] = []
    researches: [Research] = []
    build_orders: [BuildOrderItem] = []

    resources_per_second: Optional[Resources] = None
    resource_history: List[Tuple[datetime.datetime, Resources]] = []
    resources: Optional[Resources] = None
    build_queue: Dict[BuildableType, List[BuildQueueItem]] = {BuildableType.BUILDING: [], BuildableType.RESEARCH: []}

    def __init__(self, planet_id: Optional[int], name: str, coords: str, is_satellite: bool):
        self.resource_history = []
        self.buildings = []
        self.researches = []
        self.resources_per_second = None
        self.resources = None
        self.build_queue = {BuildableType.BUILDING: [], BuildableType.RESEARCH: []}
        self.id = planet_id
        self.name = name
        self.coords = coords
        self.is_satellite = is_satellite

    def __repr__(self):
        return 'Planet id: %d name: %s coords: %s' % (self.id if self.id is not None else -1, self.name, self.coords)

    def buildable_from_build_order_item(self, build_order_item: BuildOrderItem) -> Optional[Buildable]:
        if build_order_item.type == BuildableType.BUILDING:
            return self.building_from_build_order_item(build_order_item)
        elif build_order_item.type == BuildableType.RESEARCH:
            return self.research_from_build_order_item(build_order_item)
        return None

    def is_building_enqueued(self, building_name) -> bool:
        return building_name in map(lambda b: b.name, self.build_queue[BuildableType.BUILDING])

    def can_build(self, buildable_type: BuildableType) -> bool:
        if buildable_type == BuildableType.RESEARCH and self.is_building_enqueued('Laboratorium Badawcze'):
            return False

        max_build_queue_size = {BuildableType.RESEARCH: 1, BuildableType.BUILDING: 3}
        return len(self.build_queue[buildable_type]) < max_build_queue_size[buildable_type]

    def has_available_resources(self, resources: Resources) -> bool:
        return self.resources >= resources

    def set_current_resources(self, resources: Resources):
        self.resources = resources
        self.resource_history.append((datetime.datetime.now(), resources))
        self.resources_per_second = ResourceCalculator.resources_per_second(self.resource_history)

    def get_next_build_order_item(self) -> Optional[BuildOrderItem]:
        for item in self.build_orders:
            buildable = None
            if item.type == BuildableType.BUILDING:
                buildable = self.building_from_build_order_item(item)
            elif item.type == BuildableType.RESEARCH:
                buildable = self.research_from_build_order_item(item)

            if buildable is None:
                continue

            if buildable.name in map(lambda b: b.name, self.build_queue[item.type]):
                continue
            if self.is_satellite and item.type == BuildableType.RESEARCH:
                continue

            if item.level > buildable.level:
                return item
        return None

    def building_from_build_order_item(self, build_order_item: BuildOrderItem) -> Optional[Building]:
        buildings = list(filter(lambda b: b.name.lower() == build_order_item.name.lower(), self.buildings))
        if len(buildings) > 0:
            return buildings[0]
        return None

    def research_from_build_order_item(self, build_order_item: BuildOrderItem) -> Optional[Research]:
        researches = list(filter(lambda b: b.name.lower() == build_order_item.name.lower(), self.researches))
        if len(researches) > 0:
            return researches[0]
        return None
