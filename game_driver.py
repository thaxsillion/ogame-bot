from game_url import GameUrl
from planet import Planet
from buildable import Buildable, BuildableType
from typing import Dict, Tuple, List, Optional

from player_profile import PlayerProfile
from resources import Resources
from research import Research
from building import Building
from build_queue_item import BuildQueueItem
from battle_hall_entry import BattleHallEntry


class GameDriver:
    def get_battle_hall(self) -> List[BattleHallEntry]:
        pass

    def wait_for_page(self):
        pass

    def navigate_to_url(self, url: GameUrl, *args):
        pass

    def navigate_to_planet(self, planet: Planet):
        pass

    def extract_satellite_planets(self, satellite_planets) -> [Planet]:
        pass

    def extract_planets(self) -> Tuple[Planet, List[Planet]]:
        pass

    def extract_player_profile(self) -> PlayerProfile:
        pass

    def build(self, buildable: Buildable) -> bool:
        pass

    def get_resources(self) -> Optional[Resources]:
        pass

    def detect_planets(self) -> List[Planet]:
        pass

    def login(self, username: str, password: str):
        pass

    def get_build_queue(self, queue_type: BuildableType) -> List[BuildQueueItem]:
        pass

    def get_researches(self) -> List[Research]:
        pass

    def get_buildings(self) -> List[Building]:
        pass

    def dump_cookies(self):
        pass
