import unittest
from typing import List, Tuple

from default_game_driver import DefaultGameDriver
from game_url import GameUrl
from planet import Planet


class TestHeadlessDriver(unittest.TestCase):

    def setUp(self):
        self.driver = DefaultGameDriver(True)

    def test_extract_planets(self):
        self.driver.navigate_to_url(GameUrl.TEST_EXTRACT_PLANETS)
        result: Tuple[Planet, List[Planet]] = self.driver.extract_planets()
        self.assertEqual(len(result[1]), 3)
        self.assertEqual(result[0].name, 'KalifatFrancuski')
        self.assertEqual(result[0].coords, '[1:293:9]')

    def test_extract_player_info(self):
        self.driver = DefaultGameDriver(True)
        self.driver.navigate_to_url(GameUrl.TEST_PLAYER_PROFILE)
        player = self.driver.extract_player_profile()

        self.assertEqual(player.username, 'Islam')
        self.assertEqual(player.sum_rank, '206')


if __name__ == '__main__':
    unittest.main()
