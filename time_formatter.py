import datetime


class TimeFormatter:
    @staticmethod
    def eta_from_seconds(seconds: int) -> str:
        d = datetime.datetime(1, 1, 1) + datetime.timedelta(seconds=seconds)
        components = ['ETA ']
        if d.day - 1 > 0:
            components.append('%dd' % (d.day - 1))
        if d.hour > 0:
            components.append('%dh' % d.hour)
        if d.minute > 0:
            components.append('%dm' % d.minute)
        if d.second > 0:
            components.append('%ds' % d.second)

        if d.day - 1 == 0 and d.hour == 0 and d.minute == 0 and d.second == 0:
            components.append('now')
        return ''.join(components)
