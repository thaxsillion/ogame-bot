import datetime
from resources import Resources
from typing import Optional, List, Tuple


class ResourceCalculator:
    @staticmethod
    def resources_per_second(history: List[Tuple[datetime.datetime, Resources]]) -> Optional[Resources]:
        history_keys = history[-16:]

        if len(history_keys) > 1:
            first_history = history_keys[0]
            last_history = history_keys[-1:][0]

            seconds = (last_history[0] - first_history[0]).seconds

            resource_diff = last_history[1] - first_history[1]
            resource_diff.crystals /= seconds
            resource_diff.metal /= seconds
            resource_diff.deuterium /= seconds
            resource_diff.energy /= seconds

            return resource_diff
        else:
            return None

    @staticmethod
    def seconds_diff(resources_per_second: Resources, target_resources) -> int:
        metal_seconds = target_resources.metal / max(1, resources_per_second.metal)
        crystals_seconds = target_resources.crystals / max(1, resources_per_second.crystals)
        deuterium_seconds = target_resources.deuterium / max(1, resources_per_second.deuterium)

        return max(metal_seconds, crystals_seconds, deuterium_seconds)
