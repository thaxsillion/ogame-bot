from resources import Resources
from buildable import Buildable, BuildableType


class Research(Buildable):
    def __init__(self, research_id: int, name: str, level: int, upgrade_costs: Resources):
        super().__init__(research_id, name, level, upgrade_costs, BuildableType.RESEARCH)

    def short_description(self):
        return u'%s %d' % (self.name, self.level)

    def description(self):
        return u'Research (%s)[id %d] Level: %d' % (self.name, self.id, self.level)

    def __str__(self):
        return self.description()

    def __repr__(self):
        return self.description()
