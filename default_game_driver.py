import re
import pickle
import simplejson
import asyncio

from game_driver import GameDriver
from game_url import GameUrl
from planet import Planet
from buildable import Buildable, BuildableType
from typing import Dict, Tuple, List, Optional

from player_profile import PlayerProfile
from resources import Resources
from research import Research
from building import Building
from build_queue_item import BuildQueueItem
from battle_hall_entry import BattleHallEntry

from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webdriver import WebDriver as RemoteWebDriver, WebElement as RemoteWebElement
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class DefaultGameDriver(GameDriver):
    driver: Optional[RemoteWebDriver] = None
    is_headless = False

    def __init__(self, is_headless: bool):
        self.is_headless = is_headless
        self.driver = self.spawn_driver(is_headless=is_headless)

    def spawn_driver(self, is_headless: bool) -> RemoteWebDriver:
        if is_headless:
            dcap = dict(DesiredCapabilities.PHANTOMJS)
            dcap["phantomjs.page.settings.userAgent"] = (
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) "
                "Chrome/41.0.2227.1 Safari/537.36")
            return webdriver.PhantomJS(desired_capabilities=dcap)
        else:
            return webdriver.Firefox()

    def wait_for_page(self):
        table = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, "leftmenu")))

    def navigate_to_url(self, url: GameUrl, *args):
        self.driver.get(url.value % args)

    def navigate_to_planet(self, planet: Planet):
        self.navigate_to_url(GameUrl.SWITCH_PLANET, planet.id)

    def login(self, username: str, password: str):
        username_field = self.driver.find_element_by_name('username')
        username_field.send_keys(username)
        password_field = self.driver.find_element_by_name('password')
        password_field.send_keys(password + Keys.RETURN)

    def extract_all_planets(self, satellite_planets: List[RemoteWebElement]) -> [Planet]:
        planets = []

        for satellite_planet in satellite_planets:
            planet_id = int(re.search('(\d+)', satellite_planet.get_attribute('onclick')).group(0))
            match = re.match('(?P<name>.*)\s\[(?P<galaxy>\d+):(?P<solar>\d+):(?P<planet>\d+)\]',
                             satellite_planet.get_attribute("innerText"), flags=re.M | re.S)
            name = match.group('name').strip()
            coords = "[%s:%s:%s]" % (match.group('galaxy'), match.group('solar'), match.group('planet'))
            planet = Planet(planet_id, name, coords, True)
            planets.append(planet)
        return planets

    def extract_planets(self) -> List[Planet]:
        planet_elements = self.driver.find_elements_by_class_name('pbox')
        planets = self.extract_all_planets(planet_elements)

        return planets

    def get_battle_hall(self) -> List[BattleHallEntry]:
        battle_hall_entries = []
        driver = self.spawn_driver(is_headless=self.is_headless)
        driver.get('https://mirkogame.pl/index.php?page=battleHall')

        entries = driver.find_elements_by_css_selector('.box-inner-content-center tbody tr')[1:]

        for entry in entries:
            tds = entry.find_elements_by_tag_name('td')

            if len(tds) == 4:
                teams = tds[1].find_elements_by_tag_name('span')

                if len(teams) == 3:
                    team1 = teams[0].text
                    team2 = teams[2].text
                    date = tds[2].text
                    units = tds[3].text.replace('.', '')

                    battle_hall_entries.append(BattleHallEntry(team1, team2, date, units))
        driver.quit()

        return battle_hall_entries

    def extract_player_profile(self) -> PlayerProfile:
        td_order_map = {
            "username": 1,
            "home_planet": 3,
            "guild": 4,
            "points_buildings": 7,
            "rank_buildings": 8,
            "points_research": 10,
            "rank_research": 11,
            "points_fleet": 13,
            "rank_fleet": 14,
            "points_defense": 16,
            "rank_defense": 17,
            "sum_points": 19,
            "sum_rank": 20,
        }
        data = self.driver.find_elements_by_tag_name('td')
        player = PlayerProfile()
        for key, value in td_order_map.items():
            setattr(player, key, data[value].get_attribute("innerText"))

        return player

    def build(self, buildable: Buildable) -> bool:
        buildable_boxes = self.driver.find_elements_by_class_name('floatbox')

        for buildable_box in buildable_boxes:
            try:
                buildable_id_elements = buildable_box.find_elements_by_tag_name('input')

                if len(buildable_id_elements) > 0:
                    buildable_id_element_array = list(
                        filter(lambda elem: elem.get_attribute('name') in ['building', 'tech'], buildable_id_elements))

                    if len(buildable_id_element_array) > 0:
                        buildable_id_element = buildable_id_element_array[0]

                        current_buildable_box_id = int(
                            buildable_id_element.get_attribute('value')) if buildable_id_element else 0
                        if current_buildable_box_id == buildable.id:
                            try:
                                button = buildable_box.find_element_by_tag_name('button')
                                button.click()
                                return True
                            except:
                                pass
            except Exception as exception:
                self.logger.log(str(exception))
                continue
        return False

    def get_resources(self) -> Optional[Resources]:
        try:
            metal = int(self.driver.find_element_by_id('current_metal').text.replace('.', ''))
            crystals = int(self.driver.find_element_by_id('current_crystal').text.replace('.', ''))
            deuterium = int(self.driver.find_element_by_id('current_deuterium').text.replace('.', ''))
            used_energy_percent = int(self.driver.find_element_by_xpath(
                "//div[contains(@class, 'resbar1')][last()]//div[@class='resbar2']//span").text[:-1])

            return Resources(crystals, deuterium, metal, used_energy_percent)
        except Exception as exception:
            self.logger.log(str(exception))
            return None

    def detect_planets(self) -> List[Planet]:
        planets = self.extract_planets()
        return planets

    def get_build_queue(self, queue_type: BuildableType) -> List[BuildQueueItem]:
        build_queue_items = []

        try:
            build_list_element = self.driver.find_element_by_id('buildlist')
            rows = build_list_element.find_elements_by_tag_name('tr')

            for row in rows:
                td_element = row.find_elements_by_tag_name('td')[0]
                matches = re.match(u'[0-9]+\.: ([a-zA-ZżźćńółęąśŻŹĆĄŚĘŁÓŃ ]+) (\d+)', td_element.text, re.UNICODE)

                name = matches.group(1)
                level = int(matches.group(2))

                build_queue_items.append(BuildQueueItem(queue_type, name, level))
        except Exception as exception:
            if not type(exception).__name__ == 'NoSuchElementException':
                self.logger.log(str(exception))
        return build_queue_items

    def get_researches(self) -> List[Research]:
        researches = []

        research_boxes = self.driver.find_elements_by_class_name('floatbox')

        for research_box in research_boxes:
            research_id = 0
            try:
                elem = research_box.find_elements_by_tag_name('input')[1]
                research_id = int(elem.get_attribute('value')) if elem else 0
            except Exception:
                pass

            full_name = research_box.find_element_by_class_name('fbtext').text
            name = re.match(u'([a-zA-ZżźćńółęąśŻŹĆĄŚĘŁÓŃ ]+)', full_name, re.UNICODE).group(0).strip()
            level = re.search('(\d+)', full_name.replace('\n', ''))
            level = int(level.group(0)) if level else 0

            cost_overflow = research_box.find_element_by_class_name('costoverflow')

            resources = cost_overflow.find_element_by_tag_name('span').find_elements_by_tag_name('span')
            images = cost_overflow.find_elements_by_tag_name('img')

            metal = 0
            crystals = 0
            deuterium = 0

            i = 0
            while i < len(resources):
                image = images[i]
                resource = resources[i]

                value = int(resource.text.replace('.', ''))

                alt = image.get_attribute('alt')

                if alt == 'Metal':
                    metal = value
                elif alt == u'Kryształ':
                    crystals = value
                elif alt == 'Deuterium':
                    deuterium = value
                i += 1

            resources = Resources(crystals, deuterium, metal, 0)
            researches.append(Research(research_id, name, level, resources))
        return researches

    def get_buildings(self) -> List[Building]:
        buildings = []
        building_boxes = self.driver.find_elements_by_class_name('floatbox')

        for building_box in building_boxes:
            building_id = 0
            try:
                elem = building_box.find_elements_by_tag_name('input')[1]
                building_id = int(elem.get_attribute('value')) if elem else 0
            except Exception:
                pass

            full_name = building_box.find_element_by_tag_name('a').text
            name = re.match(u'([a-zA-ZżźćńółęąśŻŹĆĄŚĘŁÓŃ ]+)', full_name, re.UNICODE).group(0)
            level = re.search('(\d+)', full_name.replace('\n', ''))
            level = int(level.group(0)) if level else 0

            cost_overflow = building_box.find_element_by_class_name('costoverflow')

            resources = cost_overflow.find_element_by_tag_name('span').find_elements_by_tag_name('span')
            images = cost_overflow.find_elements_by_tag_name('img')

            metal = 0
            crystals = 0
            deuterium = 0

            i = 0
            while i < len(resources):
                image = images[i]
                resource = resources[i]

                value = int(resource.text.replace('.', ''))

                alt = image.get_attribute('alt')

                if alt == 'Metal':
                    metal = value
                elif alt == u'Kryształ':
                    crystals = value
                elif alt == 'Deuterium':
                    deuterium = value
                i += 1

            resources = Resources(crystals, deuterium, metal, 0)
            buildings.append(Building(building_id, name, level, resources))
        return buildings

    def dump_cookies(self):
        cookies = self.driver.get_cookies()
        with open("cookies.pkl", 'wb') as fh:
            pickle.dump(cookies, fh)
        with open("cookies.json", 'w') as fh:
            simplejson.dump(cookies, fh)

    def debug_print_source(self):
        print("SOURCE: %s" % self.driver.page_source)

    def debug_save_source(self, filename='debug.html'):
        with open(filename, 'w', encoding='utf8') as f:
            f.write(self.driver.page_source)
