class BattleHallEntry:
    team1: str = ''
    team2: str = ''
    date: str = ''
    units: int = 0

    def __init__(self, team1: str, team2: str, date: str, units: int):
        self.team1 = team1
        self.team2 = team2
        self.date = date
        self.units = units

    def __eq__(self, other):
        return self.team1 == other.team1 and self.team2 == other.team2 \
            and self.date == other.date and self.units == other.units

    def __repr__(self):
        return 'Battle hall entry team1: %s team2: %s date %s units: %d' % (self.team1, self.team2, self.date, self.units)
