from typing import Optional
from buildable import BuildableType


class BuildQueueItem:
    type: Optional[BuildableType] = None
    name: str = u''
    level: int = 0

    def __init__(self, buildable_type: BuildableType, name, level):
        self.type = buildable_type
        self.name = name
        self.level = level

    def __repr__(self):
        return 'BuildQueueItem %s %s %d' % (self.type, self.name, self.level)
