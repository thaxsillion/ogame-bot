from resources import Resources
from enum import Enum
from typing import Optional


class BuildableType(Enum):
    BUILDING = 'BUILDINGS'
    RESEARCH = 'RESEARCH'


class Buildable:
    name: str = u''
    level: int = 0
    id: int = 0
    upgrade_costs: Optional[Resources] = None
    type: Optional[BuildableType] = None

    def __init__(self, buildable_id: int, name: str, level: int, upgrade_costs: Resources, buildable_type: BuildableType):
        self.id = buildable_id
        self.name = name
        self.level = level
        self.upgrade_costs = upgrade_costs
        self.type = buildable_type
